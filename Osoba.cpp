#include "Osoba.h"

using namespace std;

Osoba::Osoba() {
	cout << "Dziala pierwszy konstrutor Osoba() bez argumentow" << endl;

	imie = "Jan";
	nazwisko = "Kowalski";
		
}

Osoba::Osoba(string imie, string nazwisko, Data b_day) {
	this->imie = imie; // this->imie odnosi si� do imie z klasy Osoba.h;  imie odnosie si� do pierwszego parametru w konstruktore powyzej
	this->nazwisko = nazwisko;
	this->b_day = b_day;

}

Osoba::Osoba(string name, string surname, int day, int month, int year) {
	this->imie = name;
	this->nazwisko = surname;
	Data data(day, month, year);

	this->b_day = data;

}

void Osoba::display() {

	cout << "Imie i nazwisko " << this->imie << " " << this->nazwisko << endl;
	this->b_day.display(); //b_day nazwa pola klasy typu Date, Date zawiera meted� display wyswietlaj�c� dat�
}

