#include "Data.h"
#include <iostream>

using namespace std;



Data::Data() :day(15), month(12), year(1899) { /* Data:: - Wskazanie zakresu, czyli klasy;Data()- odniesienie si� do konkretnego konstruktora z pliku Data.h;
	:day(15), month(14), year(1899) - ustawinie pol klasy day, month year wartosciami wpisanymi z palca*/
}


/* Data::Data() {
day = 15;
month = 12;
year = 1899; // Usatwaimy pola na wartosci wpisane z palca drugim sposobem przez przypisanie - pierwszy sposob by� przez inicjalizacje, akapi wyzej
}
*/


Data::Data(int day, int month, int year) : day(day), month(month), year(year) { /* Data:: - Wskazanie zakresu, czyli klasy; Data(int day, int month, int year) - odniesienie si� do konkretnego konstruktora z pliku Data.h
	: day(day), month(month), year(year) - zainicjalizowanie p�l: day, month, year wartosciami parametr�w day, month, year*/
}




int Data::getDay() { // getDay - skladnia gettera + nazwa pola kt�re wyci�gamy, tylko, ze z duzej litery; int si� odnosi do tego co bedzie zwracane
	return this->day; //this-> podczas kompilacji tym this jest to na czym wywolujemy metode (konkretny obiekt)
}
int Data::getMonth() {
	return this->month;
}
int Data::getYear() {
	return this->year;
}

void Data::setDay(int day) { // obowiazkowo w nawiasie trzeba podac typ parametru i jego nazwe z malej
	this->day = day;
}
void Data::setMonth(int month) {
	this->month = month;
}
void Data::setYear(int year) {
	this->year = year;
}

void Data::display() {

	cout << this->day << " " << this->month << " " << this->year << endl; // 

}