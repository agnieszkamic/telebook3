#include "Firma.h"
#include <iostream>

using namespace std;

Firma::Firma() {
    cout << "Dzia�a konstruktor bez arg." << endl;

    nazwa = "AMConsulting";
    adres = "Pozimkowa 14";
    kontakt = "Email: amqpi@gmail.com";
    // Inicjalizujemy dat� utworzenia domy�ln� dat�
    dataUtworzenia = Data();
}

// Konstruktor z parametrami
Firma::Firma(string nazwa, string adres, string kontakt, Data dataUtworzenia) : nazwa(nazwa), adres(adres), kontakt(kontakt), dataUtworzenia(dataUtworzenia) {}

// Getter i setter dla daty utworzenia
void Firma::setDataUtworzenia(Data data) {
    dataUtworzenia = data;
}

Data Firma::getDataUtworzenia() {
    return dataUtworzenia;
}

void Firma::display() {
    cout << "Dane firmy: " << nazwa << " " << adres << " " << kontakt << " " << endl;
    dataUtworzenia.display();
}
