#pragma once
#include <iostream>
#include "Data.h"


using namespace std;
class Osoba {

	string imie;
	string nazwisko;
	Data b_day;


	public:

		Osoba();
		Osoba(string imie, string nazwisko, Data b_day);
		Osoba(string imie, string nazwisko, int day, int month, int year);

		void display();
};