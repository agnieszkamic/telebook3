﻿

#include <iostream>
using namespace std;
#include "Data.h"
#include "Osoba.h"
#include "Firma.h"


int main()
{
	Data data1; // Wykorzystanie konstruktora Data() z pliku Data.h
	//Data data2(06, 05, 2005); // Wykorzystanie konstruktora Data(int day, int month, int year) z pliku Data.h

	data1.display(); //data1 jest this-> z pliku Data.cpp
	//data2.display(); //data2 jest this-> z pliku Data.cpp

	/*cout << data1.getDay() << " " << data1.getMonth() << " " << data1.getYear() << endl;
	cout << data2.getDay() << " " << data2.getMonth() << " " << data2.getYear() << endl;*/


/*	data1.setDay(01);
	data1.setMonth(02);
	data1.setYear(2018);
	data1.display();
	// cout << data1.getDay() << " " << data1.getMonth() << " " << data1.getYear() << endl;

	data2.setDay(10);
	data2.setMonth(12);
	data2.setYear(2000);
	data2.display();
	// cout << data2.getDay() << " " << data2.getMonth() << " " << data2.getYear() << endl;*/

	Osoba moja_osoba; // wywołasnie pierwszego konstruktora z samymi nawiasami z klasy Osoba (pliki cpp i h)
	moja_osoba.display();
	Firma mojaFirma;
	mojaFirma.display();

	Data nowaData(1, 1, 2022);
	mojaFirma.setDataUtworzenia(nowaData);

	//Osoba moja_osoba2("Agnieszka", "Kowalska", data2);
	//moja_osoba2.display();

	// Wyświetlenie danych firmy po zmianie daty utworzenia
	mojaFirma.display();

	return 0;
}




/* Pytania:
- Dleczego w Osoba.cpp mamy settery i gettery, a w klasie Data.cpp juz nie? skoro w jednym i drugim przypadku mamy atrybuty private*/



