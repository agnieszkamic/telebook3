#pragma once
#include "Data.h"
#include <string>

using namespace std;

class Firma {
    string nazwa;
    string adres;
    string kontakt;
    Data dataUtworzenia; // Dodajemy obiekt klasy Data do przechowywania daty utworzenia firmy

public:
    Firma(); // Konstruktor domyślny
    Firma(string nazwa, string adres, string kontakt, Data dataUtworzenia); // Konstruktor z parametrami

    // Getter i setter dla daty utworzenia
    void setDataUtworzenia(Data data);
    Data getDataUtworzenia();

    void display(); // Wyświetlanie danych firmy
};
